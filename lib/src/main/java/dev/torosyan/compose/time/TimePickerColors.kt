package dev.torosyan.compose.time

import androidx.compose.ui.graphics.Color

/**
 * A color scheme for use with a [TimerTimePicker] or a [RoundButton].
 */
class TimePickerColors(
    /**
     * The color of a [RoundButton] when pressed.
     */
    var pressedColor: Color,
    /**
     * The color of a [RoundButton].
     */
    var defaultColor: Color,
    /**
     * The color of a [RoundButton]'s text when pressed.
     */
    var pressedTextColor: Color,
    /**
     * The color of a [RoundButton]'s text.
     */
    var defaultTextColor: Color
)