@file:Suppress("RedundantVisibilityModifier")

package dev.torosyan.compose.time

import android.view.MotionEvent
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.*
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

private const val expandMs = 50
// These values are from AOSP
private const val expandColorMs = 50
private const val contractMs = 417
private const val contractDelay = 33

private val expandColorSpec = tween<Color>(
    durationMillis = expandColorMs,
    delayMillis = 0,
    easing = LinearEasing
)
private val expandRadiusSpec = tween<Dp>(
    durationMillis = expandMs,
    delayMillis = 0,
    easing = LinearEasing
)

private val contractColorSpec = tween<Color>(
    durationMillis = contractMs,
    delayMillis = contractDelay,
    easing = LinearEasing
)
private val contractRadiusSpec = tween<Dp>(
    durationMillis = contractMs,
    delayMillis = contractDelay,
    easing = FastOutSlowInEasing
)

/**
 * Generates appropriate [TimePickerColors] for light or dark mode, based on Material 3.
 *
 * @param darkMode if true, uses colors optimized for dark mode
 * @return [TimePickerColors] based on the mode
 */
@Composable
public fun generateM3Colors(darkMode: Boolean): TimePickerColors {
    // these match up well with the lock screen
    return if (darkMode) TimePickerColors(
        pressedColor = MaterialTheme.colorScheme.primary,
        defaultColor = MaterialTheme.colorScheme.surfaceColorAtElevation(1.dp),
        pressedTextColor = MaterialTheme.colorScheme.surfaceColorAtElevation(1.dp),
        defaultTextColor = MaterialTheme.colorScheme.onSurfaceVariant
    )
    else TimePickerColors(
        pressedColor = MaterialTheme.colorScheme.primaryContainer,
        defaultColor = MaterialTheme.colorScheme.surfaceColorAtElevation(1.dp),
        pressedTextColor = MaterialTheme.colorScheme.onSurfaceVariant,
        defaultTextColor = MaterialTheme.colorScheme.onSurfaceVariant
    )
}

/**
 * A round button similar to the ones in the PIN keyboard on Android 13's lock screen.
 *
 * @param onClick called when the button is pressed
 * @param colors a color scheme to use for the button
 */
@OptIn(ExperimentalComposeUiApi::class)
@Composable
public fun RoundButton(
    onClick: () -> Unit,
    colors: TimePickerColors = generateM3Colors(false),
    content: @Composable (RowScope.() -> Unit)
) {
    // Define a mutable state for the button being pressed down
    val pressed = remember {mutableStateOf(false)}
    val radiusDp = if (pressed.value) 10.dp else 40.dp

    // Animated values
    val bgColor: Color by animateColorAsState(
        if (pressed.value) colors.pressedColor else colors.defaultColor,
        animationSpec = if (pressed.value) expandColorSpec else contractColorSpec
    )
    val txColor: Color by animateColorAsState(
        if (pressed.value) colors.pressedTextColor else colors.defaultTextColor,
        animationSpec = if (pressed.value) expandColorSpec else contractColorSpec
    )
    val radius = animateDpAsState(
        radiusDp,
        animationSpec = if (pressed.value) expandRadiusSpec else contractRadiusSpec
    )
    Button(
        onClick = { /* this is overridden by Modifier.pointerInteropFilter */ },
        modifier = Modifier
            .padding(5.dp)
            .size(80.dp)
            .pointerInteropFilter {
                // this is here to change values when the button is pressed down
                when (it.action) {
                    MotionEvent.ACTION_DOWN -> {
                        pressed.value = true
                    }

                    MotionEvent.ACTION_UP -> {
                        pressed.value = false
                        onClick()
                    }
                }
                true
            },
        colors = ButtonDefaults.buttonColors(
            containerColor = bgColor,
            contentColor = txColor
        ),
        content = content,
        shape = RoundedCornerShape(radius.value)
    )
}


/**
 * A round button similar to the ones in the PIN keyboard on Android 13's lock screen.
 * This is a [RoundTextButton] but with a number as the content.
 *
 * @param number the number to show
 * @param onClick called when the button is pressed, the argument is the number
 * @param colors a color scheme to use for the button
 */
@Composable
public fun NumberButton(
    number: Int,
    colors: TimePickerColors = generateM3Colors(false),
    onClick: (Int) -> Unit
) {
    RoundTextButton(number.toString(), colors) { onClick(number) }
}

/**
 * A round button similar to the ones in the PIN keyboard on Android 13's lock screen.
 * This is a [RoundButton] but with text as the content.
 *
 * @param onClick called when the button is pressed
 * @param text the content of the button
 */
@Composable
public fun RoundTextButton(
    text: String,
    colors: TimePickerColors = generateM3Colors(false),
    onClick: () -> Unit
) {
    RoundButton(onClick = onClick, colors) {
        Text(
            text,
            fontSize = 30.sp,
            fontWeight = FontWeight.Normal
        )
    }
}