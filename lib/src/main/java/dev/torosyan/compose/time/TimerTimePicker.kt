@file:Suppress("RedundantVisibilityModifier")

package dev.torosyan.compose.time

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog

/**
 * A timer-style time picker dialog.
 *
 * @param onDismiss called when the user wants to dismiss the dialog, e.g. by pressing the Close button
 * @param onExit called when the user presses the OK button after setting the time, the argument is the set time in minutes
 * @param colors the time picker's color scheme
 */
@Composable
public fun TimerTimePicker(
    onDismiss: () -> Unit,
    onExit: (Long) -> Unit,
    colors: TimePickerColors = generateM3Colors(darkMode = false)
) {

    // this is a quick and dirty way to achieve the same behavior as
    // most timer apps
    val time = remember { mutableStateOf("0000")}

    Dialog(
        onDismissRequest = { onDismiss() }
    ) {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            Column(
                Modifier.fillMaxWidth()
            ) {
                // top container
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(100.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center,
                ) {
                    Text(
                        text = String.format(
                            stringResource(R.string.time_picker_text),
                            time.value.substring(0, 2),
                            time.value.substring(2, 4)
                        ),
                        fontSize = 30.sp
                    )
                }

                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(top = 10.dp)
                ) {
                    Row(Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center) {
                        NumberButton(1, colors) {addToTime(time, it.toString())}
                        NumberButton(2, colors) {addToTime(time, it.toString())}
                        NumberButton(3, colors) {addToTime(time, it.toString())}
                    }
                    Row(Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center) {
                        NumberButton(4, colors) {addToTime(time, it.toString())}
                        NumberButton(5, colors) {addToTime(time, it.toString())}
                        NumberButton(6, colors) {addToTime(time, it.toString())}
                    }
                    Row(Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center) {
                        NumberButton(7, colors) {addToTime(time, it.toString())}
                        NumberButton(8, colors) {addToTime(time, it.toString())}
                        NumberButton(9, colors) {addToTime(time, it.toString())}
                    }
                    Row(Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center) {
                        RoundButton({time.value = "0000"}, colors) {
                            Image(
                                Icons.Filled.Clear, stringResource(R.string.time_picker_clear),
                                colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.onSurfaceVariant)
                            )
                        }
                        NumberButton(0, colors) {addToTime(time, it.toString())}
                        RoundButton({removeFromTime(time)}, colors) {
                            Image(
                                painterResource(id = R.drawable.ic_backspace), stringResource(R.string.time_picker_backspace),
                                colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.onSurfaceVariant)
                            )
                        }
                    }
                }

                Row(Modifier.padding(10.dp)) {
                    OutlinedButton(
                        onClick = { onDismiss() },
                        Modifier
                            .fillMaxWidth()
                            .padding(8.dp)
                            .weight(1F)
                    ) {
                        Text(text = stringResource(R.string.time_picker_cancel))
                    }

                    Button(
                        onClick = {
                            // turn time string into hours and minutes
                            val hours = time.value.substring(0, 2).toInt()
                            val minutes = time.value.substring(2, 4).toInt()
                            onExit((hours*60 + minutes).toLong())
                        },
                        Modifier
                            .fillMaxWidth()
                            .padding(8.dp)
                            .weight(1F),
                        enabled = (time.value != "0000")
                    ) {
                        Text(text = stringResource(R.string.time_picker_ok))
                    }
                }


            }
        }
    }
}

fun removeFromTime(time: MutableState<String>) {
    // remove leading zeroes and last number
    var shortTime = time.value.substring(0, 3).trimStart { it == '0' }
    // add back leading zeroes
    shortTime = shortTime.padStart(4, '0')
    // update state
    time.value = shortTime
}

fun addToTime(time: MutableState<String>, it: String) {
    Log.d("blocker", "time value: ${time.value}")
    // remove leading zeroes
    var shortTime = time.value.trimStart { it == '0' }
    // add requested number
    shortTime += it
    // add back leading zeroes
    shortTime = shortTime.padStart(4, '0')
    Log.d("blocker", "shortTime: $shortTime")
    // update state
    time.value = shortTime
    Log.d("blocker", "new time value: ${time.value}")
}
