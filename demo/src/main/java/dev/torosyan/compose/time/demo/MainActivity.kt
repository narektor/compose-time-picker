package dev.torosyan.compose.time.demo

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import dev.torosyan.compose.time.TimePickerColors
import dev.torosyan.compose.time.generateM3Colors
import dev.torosyan.compose.time.RoundTextButton
import dev.torosyan.compose.time.TimerTimePicker
import dev.torosyan.compose.time.demo.ui.theme.ComposetimepickerTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            // State for opening the time picker
            val showPicker = remember { mutableStateOf(false)}
            // Remember the time set
            val time = remember { mutableStateOf(0L)}
            ComposetimepickerTheme {

                // To get a color scheme you can generate one using a
                // method provided by the library
                val material3Colors = generateM3Colors(darkMode = isSystemInDarkTheme())
                // Or use a custom one
                val customColors = TimePickerColors(
                    pressedColor = Color.Blue,
                    defaultColor = Color.White,
                    pressedTextColor = Color.White,
                    defaultTextColor = Color.Black,
                )

                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {
                        var text = "Remind me in:"
                        if (time.value > 0) {
                            text = if (time.value == 1L) "Reminding in a minute."
                            else "Reminding in ${time.value} minutes."
                        }
                        Text(text)
                        Button(onClick = { showPicker.value = true }) {
                            Text("Set time")
                        }

                        Row(verticalAlignment = Alignment.CenterVertically) {
                            RoundTextButton(text = "M", colors = material3Colors) {
                                Toast
                                    .makeText(applicationContext, "Material 3 color scheme", Toast.LENGTH_SHORT)
                                    .show()
                            }
                            RoundTextButton(text = "C", colors = customColors) {
                                Toast
                                    .makeText(applicationContext, "Custom color scheme", Toast.LENGTH_SHORT)
                                    .show()
                            }
                            RoundTextButton(text = "D") {
                                Toast
                                    .makeText(applicationContext, "Default color scheme", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                    }
                    if (showPicker.value) {
                        TimerTimePicker(
                            // When dismissed, hide the time picker.
                            onDismiss = { showPicker.value = false },
                            // When time is set, save it.
                            onExit = {
                                // But first, hide the time picker.
                                showPicker.value = false
                                time.value = it
                                Toast.makeText(this, "Reminder set.", Toast.LENGTH_SHORT).show()
                            },
                            // Pass in a color scheme.
                            material3Colors
                        )
                    }
                }
            }
        }
    }
}