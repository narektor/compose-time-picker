# Compose Time Picker

This Jetpack Compose library provides a timer style time picker component, designed to mimic the Android 13 lock screen's PIN keyboard.

<img src="screenshots/TimerTimePicker.png" width="300px" alt="A screenshot of the time picker. From top to bottom,  it has large text saying '00h 00m', a grid of circular buttons with numbers and 2 round rectangle buttons at the bottom: an outlined button saying Cancel and a disabled one saying OK.">

## Showcase

This library has been used in:
- [Disconnect](https://gitlab.com/narektor/disconnect)

If you want to add your app here please let me know by making an issue and adding the [showcase] label.

## Installation

You can install this library from JitPack. In your **root** `build.gradle`, add the JitPack repository:

```gradle
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

Then, in your **app module's** `build.gradle`, import the library.

```gradle
dependencies {
    implementation 'com.gitlab.narektor:compose-time-picker:(latest release)'
}
// To find out the latest release see the Releases tab,
// or the "Latest Release" badge at the top.
```

## Usage

This library provides a timer style time picker, which you can see at the top of the README, under the `TimerTimePicker` class. The usage is similar to a regular Material 3 `Dialog`. Here's an example:

```kotlin
// Import the library:
import dev.torosyan.compose.time.TimerTimePicker

// Then, in your activity:
TimerTimePicker(
    onDismiss = {
        // Called when the time picker should be closed.
        // You should hide the dialog here.
    },
    onExit = {
        // Called when the user presses the OK button after setting the time.
        // The set time, in minutes, is provided as the argument. For example, let's show it:
        Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
    }
)
```

For a more complete example see the [demo app](https://gitlab.com/narektor/compose-time-picker/-/tree/main/demo).
