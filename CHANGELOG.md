# Changelog

## 1.0.1
Fix critical issues with 1.0. **If you're using 1.0, it's strongly advised to update to 1.0.1**

## 1.0
Initial release.
